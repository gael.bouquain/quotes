import { StyleSheet } from "react-native"

export let background = "#202125"
export let textColor = "#007daa"

let style = StyleSheet.create({
	box: {
		alignItems: "center",
		justifyContent: "center",
		fontFamily: "Manjari-Bold",
		backgroundColor: background,
	},
	container: {
		flex: 1,
		textAlign: "center",
		alignItems: "center",
		alignContent: "center",
		justifyContent: "center",
		fontFamily: "Manjari-Bold",
		backgroundColor: background,
	},
	Text: {
		margin: 10,
		fontSize: 40,
		marginTop: 20,
		color: textColor,
		borderRadius: 200,
		fontFamily: "Manjari-Bold",
	},
	text: {
		margin: 10,
		fontSize: 30,
		color: textColor,
		textAlign: "center",
		fontFamily: "Manjari-Bold",
	},
	Symbole: {
		margin: 0,
		fontSize: 40,
		marginTop: -20,
		color: textColor,
		fontFamily: "Manjari-Bold",
	},
	Number: {
		fontSize: 30,
		marginTop: -7,
		color: "black",
		marginLeft: -45,
		fontFamily: "Manjari-Bold",
	},
	element: {
		width: 100,
		height: 100,
		borderWidth: 5,
		borderRadius: 10,
		alignItems: "center",
		borderColor: "black",
	},
	button: {
		margin: 25,
		paddingTop: 2.3,
		elevation: 2,
		paddingBottom: 0,
		borderRadius: 300,
		alignItems: "center",
		paddingHorizontal: 10,
	},
	label: {
		fontSize: 20,
		color: "#125a82",
		fontFamily: "Manjari-Bold",
	},
	drawer: {
		width: 130,
		height: 130,
		marginTop: 30,
		marginBottom: 20,
		marginLeft: "auto",
		marginRight: "auto",
	},
	textOne: {
		left: 5,
		top: 85,
		margin: 0,
		margin: 10,
		fontSize: 20,
		color: "#ae7156",
		alignItems: "center",
		position: "absolute",
		fontFamily: "Manjari-Bold",
	},
	textTwo: {
		fontSize: 20,
		color: textColor,
	},
	textInput: {
		fontSize: 20,
		borderRadius: 10,
		color: textColor,
		borderColor: textColor,
		fontFamily: "Manjari-Bold",
	},
	textInputAdd: {
		fontSize: 20,
		borderWidth: 1,
		borderRadius: 20,
		color: textColor,
		borderColor: textColor,
		fontFamily: "Manjari-Bold",
	},
})

export default style
