import React from "react"
import {
	View,
	Text,
	StatusBar,
	ActivityIndicator,
    Dimensions,
    Image,
    TouchableNativeFeedback,
} from "react-native"
import { RandomGradientButton } from "linear-gradient-button"
import style from "./style"
import { DarkModalCustom } from "react-native-dark-modal"
import Tts from 'react-native-tts'
import {
    WaveIndicator,
} from 'react-native-indicators'

class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			quotes: "Tu n'as pas cliquer",
			modalVisible: false,
            animating: false,
            count: 0,
		}
	}

	top = (Dimensions.get("screen").height - 240) / 2
	left = (Dimensions.get("screen").width - 310) / 2

    ModalVisible() {
		this.setState({ modalVisible: !this.state.modalVisible })
	}

    getQuotes = async () => {

        let timeFirst = new Date().getTime()
        let time

        let intervale = setInterval(() => {
            let timeSecond = new Date().getTime()

            time = timeSecond - timeFirst

            console.log(time)

        }, 100)
        
        if (time >= 10000) {
            clearInterval(intervale)
            return "Le serveur à mis trop de temps à répondre"
        }

		let requestOptions = {
			method: "GET",
			redirect: "follow",
		}

		let fetchVariable = await fetch(
			"https://freequote.herokuapp.com/",
			requestOptions
		)
			.then((response) => response.text())
			.then((result) => JSON.parse(result))
            .catch(() => {
                if (this.state.count >= 1) {
                    this.setState({animating: true})
                    setTimeout(() => {
                        this.setState({animating: false})
                        return "Le serveur ne répond pas"
                    }, 1000)
                } else {
                    this.toggleModalVisible()
                }
            }) 

        if (fetchVariable != undefined) {

            clearInterval(intervale)

            this.setState({ animating: false })
            
            return fetchVariable
            
        } else if (fetchVariable == undefined || fetchVariable == null) {
            
            clearInterval(intervale)

            this.setState({ animating: false })
            
			return "Il y a eu un problème"
		}
	}

	setQuotes = async () => {

        if (this.state.modalVisible === true) {
            this.toggleModalVisible()
        }
        
		this.setState({ animating: true })
        
        let quote = await this.getQuotes()

        console.log(typeof quote)
        
		console.log("quote:", quote)
		this.setState({
			quotes:
				quote || "Il y a eu un problème"
        })
    }
    
    speak = async () => {
        
        if (this.state.quotes?.quote) { 
            Tts.setDefaultLanguage("en-US")

            Tts.setDucking(true)

            Tts.speak(this.state.quotes.quote, {
                androidParams: {
                    KEY_PARAM_STREAM: 'STREAM_MUSIC',
                },
            })
        } else {
            Tts.setDefaultLanguage("fr-FR")

            Tts.setDucking(true)
            
            Tts.speak("Tu n'as pas cliquer", {
                androidParams: {
                    KEY_PARAM_STREAM: 'STREAM_MUSIC',
                },
            })
        }
    }


	render() {
		return (
			<View style={style.container}>
				<StatusBar hidden={true} />

				<Text style={[style.Text, { textAlign: 'center' }]}>
					{this.state.quotes?.quote || this.state.quotes}
				</Text>

				<View style={{ position: 'absolute' }}>
					<WaveIndicator
						animating={this.state.animating}
						style={{ elevation: 100 }}
						size={100}
						color="#00c9b7"
					/>
				</View>

				<TouchableNativeFeedback onPress={() => this.speak()}>
					<Image
						style={{
							position: 'absolute',
							top: 18,
							left: Dimensions.get('screen').width - 150,
							width: 60,
							height: 60,
						}}
						source={require('./sound.png')}
					/>
				</TouchableNativeFeedback>

				<TouchableNativeFeedback
					onPress={() => {
						this.setState({
							modalText:
								this.state.quotes?.author == undefined
									? "Il n'y a pas de citation"
									: `Auteur:\n${this.state.quotes.author}`,
						})
						this.toggleModalVisible()
					}}
				>
					<Image
						style={{
							position: 'absolute',
							top: 18,
							left: Dimensions.get('screen').width - 70,
							width: 60,
							height: 60,
						}}
						source={require('./author.png')}
					/>
				</TouchableNativeFeedback>

				<DarkModalCustom
					style={{
						width: 310,
						top: this.top,
						height: 270,
						left: this.left,
						elevation: 300,
						borderRadius: 20,
						alignItems: 'center',
						backgroundColor: '#202125',
						justifyContent: 'space-around',
					}}
					visible={this.state.modalVisible}
				>
					<Text style={style.text}>
						{this.state.modalText ||
							`Est tu sur d'avoir activés le WI-FI ?`}
					</Text>

					<RandomGradientButton
						style={[style.button, { marginBottom: 8 }]}
						onPress={() => {
							this.state.count++
							this.toggleModalVisible()
						}}
					>
						<Text style={style.text}>{`fermer`}</Text>
					</RandomGradientButton>
				</DarkModalCustom>

				<RandomGradientButton
					onPress={async () => await this.setQuotes()}
					style={[style.button, { width: 200, height: 60 }]}
				>
					<Text
						style={[
							style.Text,
							{
								margin: 10,
								fontSize: 40,
								marginTop: 20,
								color: 'black',
								marginBottom: -10,
								borderRadius: 200,
								marginHorizontal: 20,
								fontFamily: 'Manjari-Bold',
							},
						]}
					>
						Citation
					</Text>
				</RandomGradientButton>
			</View>
		)
	}
}

export default App
